const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const createFileRouter = require('./apiRouters/createFileRouter');
const getFilesRouter = require('./apiRouters/getFilesRouter');
const getFileRouter = require('./apiRouters/getFileRouter');
const editFileRouter = require('./apiRouters/editFileRouter');
const deleteFileRouter = require('./apiRouters/deleteFileRouter');
const path = require('path');
const app = express();
const PORT = 8080;
const validExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logs.log'), { flags: 'a' });

try {
    fs.mkdir(`api/files`,{ recursive: true }, (err) => {
        if (err) {
            throw err;
        }
    })
} catch (e) {
    console.log(e);
}

app.use(express.json());
app.use(morgan('tiny', { stream: accessLogStream }));

app.use('/api/files', createFileRouter);
app.use('/api/files', getFilesRouter);
app.use('/api/files', getFileRouter);
app.use('/api/files', editFileRouter);
app.use('/api/files', deleteFileRouter);

app.listen(PORT);