const fs = require('fs');
const express = require('express');
const getFileRouter = express.Router();
const path = require('path');

getFileRouter.get('/:filename', (req, res) => {
    const { filename } = req.params;

    if (!filename) {
        res.status(400).send({ "message": `No file with ${filename} found` })
    } else {

        let isExists = false;

        fs.readdir(path.join(__dirname, `../api/files/`), (err, data) => {

            let existingFile = null;;

            if (err) {
                res.status(500).send({ "message": "Server error" });
            } else {
                existingFile = data.find(item => item === filename);
            }

            if (existingFile) {

                let ext = filename.split('.');
                ext = ext[ext.length - 1]

                try {
                    fs.readFile(path.join(__dirname, `../api/files/${filename}`), 'utf-8', (err, data) => {

                        res.status(200).send(
                            {
                                "message": "Success",
                                "filename": filename,
                                "content": data || '',
                                "extension": ext,
                                "uploadedDate": new Date().toISOString()
                            }
                        )
                    });
                } catch (e) {
                    res.status(500).send({ "message": "Server error" });
                }
            } else {

                res.status(400).send({ "message": `No file with ${filename} found` })
            }
        });

    }
})

module.exports = getFileRouter;