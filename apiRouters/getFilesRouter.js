const fs = require('fs');
const express = require('express');
const getFilesRouter = express.Router();
const path = require('path');

getFilesRouter.get('/', (req, res) => {

    try {

        fs.readdir(path.join(__dirname, `../api/files/`), (err, data) => {
            if (err) {
                throw err;
            }

            if (data.length) {
                res.status(200).send(
                    {
                        "message": "Success",
                        "files": data
                    }
                )

            } else {
                res.status(400).send({ "message": "Client error" })
            }

        });

    } catch (e) {
        res.status(500).send({ "message": "Server error" });
    }
})

module.exports = getFilesRouter;