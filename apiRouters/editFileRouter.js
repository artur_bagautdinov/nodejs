const fs = require('fs');
const express = require('express');
const editFileRouter = express.Router();
const path = require('path');

editFileRouter.patch('/:filename', (req, res) => {
    const { filename } = req.params;
    const { content } = req.body;

    if (!filename) {
        res.status(400).send({ "message": `No file with ${filename} found` })
    } else {

        let isExists = false;

        fs.readdir(path.join(__dirname, `../api/files/`), (err, data) => {

            let existingFile = null;;

            if (err) {
                res.status(500).send({ "message": "Server error" });
            } else {
                existingFile = data.find(item => item === filename);
            }

            if (existingFile) {

                try {
                    fs.writeFile(path.join(__dirname, `../api/files/${filename}`), content, 'utf-8', (err, data) => {

                        res.status(200).send(
                            {
                                "message": "Successfully modyfied",
                                "filename": filename,
                                "modyfiedDate": new Date().toISOString()
                            }
                        )
                    });
                } catch (e) {
                    res.status(500).send({ "message": "Server error" });
                }
            } else {

                res.status(400).send({ "message": `No file with ${filename} found` })
            }
        });

    }
})

module.exports = editFileRouter;