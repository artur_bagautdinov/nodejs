const fs = require('fs');
const express = require('express');
const deleteFileRouter = express.Router();
const path = require('path');

deleteFileRouter.delete('/:filename', (req, res) => {
    const { filename } = req.params;

    if (!filename) {
        res.status(400).send({ "message": `No file with ${filename} found` })
    } else {

        let isExists = false;

        fs.readdir(path.join(__dirname, `../api/files/`), (err, data) => {

            let existingFile = null;;

            if (err) {
                res.status(500).send({ "message": "Server error" });
            } else {
                existingFile = data.find(item => item === filename);
            }

            if (existingFile) {

                try {
                    fs.unlink(path.join(__dirname, `../api/files/${filename}`), (err, data) => {

                        res.status(200).send(
                            {
                                "message": "Successfully deleted",
                                "filename": filename,
                                "deletedDate": new Date().toISOString()
                            }
                        )
                    });

                } catch (e) {
                    res.status(500).send({ "message": "Server error" });
                }

            } else {

                res.status(400).send({ "message": `No file with ${filename} found` })
            }
        });

    }
})

module.exports = deleteFileRouter;