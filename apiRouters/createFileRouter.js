const fs = require('fs');
const express = require('express');
const createFileRouter = express.Router();
const path = require('path');

const validExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

function checkValidExt(filename) {
    let newExtension = filename.split('.');
    newExtension = newExtension[newExtension.length - 1]
    const ext = !!validExtensions.find(item => item === newExtension);

    return ext;
}

createFileRouter.post('/', (req, res) => {

    const { filename, content } = req.body;

    const isExtValid = checkValidExt(filename);

    if (!isExtValid || filename.length === 0) {

        res.status(400).send({ "message": "Please specify 'content' parameter" });

    } else {

        try {
            fs.writeFile(path.join(__dirname, `../api/files/${filename}`), content, (err) => {
                if (err) {
                    throw err;
                }

                res.status(200).send({ "message": "File created successfully" })
            });

        } catch (e) {
            res.status(500).send({ "message": "Server error" })
        }

    }
})

module.exports = createFileRouter;